'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;
var TrimmedString = {type:String, trim:true};

/**
 * Timesheet Schema
 */
var TimesheetSchema = new Schema({
	start: TrimmedString,
    end: TrimmedString,
    status:TrimmedString,
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    project: {
        type: Schema.ObjectId,
        ref: 'Project'
    },
    daily:[
        {
            date:TrimmedString,
            hours: Number,
            comment: TrimmedString,
            _id:false
        }
    ],
    docs: [{
        type: TrimmedString,
        file: TrimmedString,
		s3error: Boolean,
        _id:false
    }],
    created: {
		type: Date,
		default: Date.now
	}
});
mongoose.model('Timesheet', TimesheetSchema);