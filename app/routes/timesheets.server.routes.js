'use strict';

module.exports = function(app) {
	var users = require('../controllers/users.server.controller');
	var timesheets = require('../controllers/timesheets.server.controller');
	var fileUpload = require('../modules/file.upload');
	var mongoose = require('mongoose');
	var Model = mongoose.model('Timesheet');

	// Timesheets Routes
	app.route('/timesheets')
		.get(users.requiresLogin,timesheets.list)
		.post(users.requiresLogin, timesheets.create);

    // Timesheets Upload Routes
    app.route('/timesheets/upload')
        .post(users.requiresLogin,fileUpload.upload(Model));
	
	app.route('/timesheets/download/:modelId/*')
		.get(fileUpload.download(Model));

    app.route('/timesheets/deleteDoc')
        .post(users.requiresLogin,fileUpload.deleteDoc(Model));

    app.route('/timesheets/:timesheetId')
		.get(users.requiresLogin,timesheets.read)
		.put(users.requiresLogin, timesheets.update)
		.delete(users.hasAuthorization(['admin']), timesheets.delete);

	// Finish by binding the Timesheet middleware
	app.param('timesheetId', timesheets.timesheetByID);
};