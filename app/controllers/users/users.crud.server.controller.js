'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Timesheet = mongoose.model('Timesheet'),
    async = require('async'),
    _ = require('lodash');

/**
 * Get the error message from error object
 */
var getErrorMessage = function(err) {
	var message = '';

	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = 'User already exists';
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	return message;
};

/**
 * Create a User
 */
exports.create = function(req, res) {
	var user = new User(req.body);
	user.user = req.user;

    // Add missing user fields
    user.provider = 'local';
    if(!user.displayName){
        user.displayName = user.firstName + ' ' + user.lastName;
    }
	user.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
            // Remove sensitive data before sending response
            user.password = undefined;
            user.salt = undefined;
			res.jsonp(user);
		}
	});
};

/**
 * Show the requested User
 */
exports.read = function(req, res) {
	res.jsonp(req.profile);
};

/**
 * Update a User
 */
exports.change = function(req, res) {
	var user = req.profile ;
    user = _.extend(user , req.body);

	user.save(function(err) {
        console.log(err);
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(user);
		}
	});
};

/**
 * Delete an User
 */
exports.delete = function(req, res) {
	var user = req.user ;

	user.remove(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(user);
		}
	});
};

/**
 * List of Users
 */
exports.list = function(req, res) { User.find({},'-salt -password -resetPasswordExpires -resetPasswordToken -provider').sort('displayName').exec(function(err, users) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(users);
		}
	});
};

/**
 * User authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.user.user.id !== req.user.id) {
		return res.send(403, 'User is not authorized');
	}
	next();
};

/**
 * Get Data related to User(Projects, Timesheets etc) based on request
 */
exports.getData = function(req,res){
    async.waterfall([
        function(done){
            User.findOne({
                _id: req.body.user || req.user._id
            }, '-salt -password -resetPasswordExpires -resetPasswordToken -provider').populate('projects.project')
                .lean(true)
                .exec(function(err, user) {
                if (!user) {
                    return res.status(400).send({
                        message: 'No account with that username has been found'
                    });
                }
                done(err, user);
            });
        },
        function(user,done){ //Populate all timesheets
            Timesheet.find({user:user._id}).lean(true)
                .sort('start project')
                .exec(function(err,timesheets){
                user.timesheets = timesheets;
                done(err,user);
            });
        },
        function(user,done){
            // TODO: Make it more user friendly.
            if(!req.body.user && _.intersection(req.user.roles, ['admin']).length){ // If logged in User is admin and request is get i.e. Homepage
                user.awaitingApprovals = user.awaitingApprovals || {};
                Timesheet.find({status:'Submitted'}).populate('project','name').populate('user', 'displayName').lean(true)
                    .sort('start project')
                    .exec(function(err,timesheets){
                        user.awaitingApprovals.timesheets = timesheets;
                        done(err,user);
                    });
            } else {
                done(null,user);
            }
        },
        function(user, done){
            res.jsonp(user);
        },
        function(err) {
            if (err) {
                return res.send(400, {
                    message: err
                });
            }
        }
    ]);
};

/**
 * Mini List of Projects
 */
exports.getList = function(req, res) { User.getList(function(err, users) {
	if (err) {
		return res.send(400, {
			message: getErrorMessage(err)
		});
	} else {
		res.jsonp(users);
	}
});
};

exports.testCall = function(req,res){
	/*
	Timesheet.find({user:'54260e186f721b0000b85fb7'}).where('likes').in(['vaporizing', 'talking']).exec(function(err,timesheets){
    });
    var periods = [{"start":"2014-08-31","end":"2014-09-06"},{"start":"2014-09-07","end":"2014-09-13"},{"start":"2014-09-14","end":"2014-09-20"},{"start":"2014-09-21","end":"2014-09-27"},{"start":"2014-09-28","end":"2014-10-04"}];
    var timesheets = [{"start":"2014-08-31","end":"2014-09-06","project":"542b9ed116b250b3386f5036","user":"542b9d1316b250b3386f5035","status":"Submitted","_id":"542d6ac9dc87980000f70382","created":"2014-10-02T15:10:01.997Z","docs":[],"daily":[{"date":"2014-08-31"},{"date":"2014-09-01","hours":8},{"date":"2014-09-02","hours":8},{"date":"2014-09-03","hours":8},{"date":"2014-09-04","hours":8},{"date":"2014-09-05","hours":8},{"date":"2014-09-06"}],"__v":0},{"start":"2014-09-07","end":"2014-09-13","project":"542b9ed116b250b3386f5036","user":"542b9d1316b250b3386f5035","status":"Saved","_id":"542d6afedc87980000f70383","created":"2014-10-02T15:10:54.161Z","docs":[],"daily":[{"date":"2014-09-07"},{"date":"2014-09-08","hours":8},{"date":"2014-09-09","hours":8},{"date":"2014-09-10","hours":8},{"date":"2014-09-11","hours":8},{"date":"2014-09-12","hours":8},{"date":"2014-09-13"}],"__v":0}];
    for(var i=-1;++i<timesheets.length;){
       var ind = _.findIndex(periods, { 'start': timesheets[i].start });
       periods[ind] = timesheets[i];
    }*/
    res.jsonp(User.modelName);
};
