'use strict';
angular.module('core').directive('oiArrayfield', function() {
    return {
      restrict: 'A',
      scope: {
        data: '=model',
        title: '@',
        fields:'=?',
        lists:'=?'
      },
      controller: ['$scope',function($scope){
        // check if it was defined.  If not - set a default
        $scope.fields = $scope.fields || [{name:'description',type:'text'},{name:'value',type:'text'}];
        $scope.addItem=function(){
            if($scope.data){
              $scope.data.push({});
            } else {
              $scope.data=[{}];
            }
        };
        $scope.removeItem = function(ind){
            $scope.data.splice(ind,1);
        };
      }],
      templateUrl: 'modules/core/utils/arrayField.html'
    };
})

.directive('oiTimesheetList',function(){
    return {
        restrict: 'E',
        templateUrl:'modules/users/views/oi-timesheetList.html'
    };
})

.directive('oiAwaitingApproval',function(){
    return {
        restrict: 'E',
        templateUrl:'modules/users/views/oi-awaitingApproval.html'
    };
})

.directive('oiDocuments',function(){
        return {
            restrict: 'EA',
            templateUrl: 'modules/core/utils/documents.html',
            scope: {
                docs: '=docs',
                functions:'=?',
                model:'=?',
                urlbase:'@'
            },
            controller: ['$scope','$modal','$http','$upload',function($scope,$modal,$http,$upload){
                $scope.files = {files:[],fileNames:[]};
                $scope.onFileSelect = function(files){
                    $scope.files.files = $scope.files.files.concat(files);
                    angular.forEach(files,function(val,key){
                        $scope.files.fileNames.push(val.name.substr(0,val.name.lastIndexOf('.')));
                    });
                    $scope.$apply();
                };

                $scope.upload = function(files){
                    $upload.upload({
                        url:$scope.urlbase+'/upload',
                        data: {_id: $scope.model._id, fileNames: files.fileNames.join('/\\')},
                        file: files.files
                    }).success(function(data, status, headers, config) {
                        $scope.model.docs = data.docs;
                        files.fileNames = [];
                        files.files = [];
                    });
                };
                $scope.deleteDoc = function(obj){
                    var data = angular.copy(obj);
                    data._id = $scope.model._id;
                    $http.post($scope.urlbase+'/deleteDoc', data).
                        success(function(data, status, headers, config) {
                            $scope.model.docs = data.docs;
                        }).
                        error(function(data, status, headers, config) {
                            alert('Error while deleting document');
                        });
                };
                if($scope.functions){
                    if($scope.functions.upload){
                        $scope.upload = $scope.functions.upload;
                    }
                    if($scope.functions.deleteDoc){
                        $scope.deleteDoc = $scope.functions.deleteDoc;
                    }
                }

                $scope.preDeleteDoc = function(obj){
                    var $childScope = $scope.$new(true);
                    $childScope.body = 'Do you want to delete this file?';
                    var modalInstance=$modal.open({
                        templateUrl:'modules/core/utils/confirmDialog.html',
                        scope:$childScope
                    });
                    modalInstance.result.then(function (status) {
                        $childScope.$destroy();
                        $scope.deleteDoc(obj);
                    }, function (reason) {
                        $childScope.$destroy();
                    });
                };

                $scope.removeFile = function(ind){
                    $scope.files.files.splice(ind,1);
                    $scope.files.fileNames.splice(ind,1);
                };
            }]
        };
    })
.controller('datepickerPopupCtrl', ['$scope', function($scope){
 $scope.toggleOpen = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = !$scope.opened;
  };
}])

.constant('globalLists',{
        payCycles:['Weekly','Bi-Weekly','Semi-Monthly', 'Monthly'],
        days:['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
        contactTypes:['Primary', 'Work', 'Personal']
});



/*
.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});*/