'use strict';

//Setting up route
angular.module('timesheets').config(['$stateProvider',
	function($stateProvider) {
        // Timesheets state routing
		$stateProvider.
		state('listTimesheets', {
			url: '/timesheets',
			templateUrl: 'modules/timesheets/views/list-timesheets.client.view.html'
		}).
		state('createTimesheet', {
			url: '/timesheets/create',
			templateUrl: 'modules/timesheets/views/create-timesheet.client.view.html'
		}).
        state('getTimesheet', {
            url: '/timesheets/get/:timesheetId',
            templateUrl: 'modules/timesheets/views/get-timesheet.cleint.view.html'
        }).
		state('viewTimesheet', {
			url: '/timesheets/:timesheetId',
			templateUrl: 'modules/timesheets/views/get-timesheet.cleint.view.html'
		}).
		state('editTimesheet', {
			url: '/timesheets/:timesheetId/edit',
			templateUrl: 'modules/timesheets/views/edit-timesheet.client.view.html'
		});
	}
]);